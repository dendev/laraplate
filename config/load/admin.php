<?php

return [
    'permissions' => [
        'backend_access' => 'backend_access',
        'view_role' => 'view_role',
        'view_any_role' => 'view_any_role',
        'create_role' => 'create_role',
        'update_role' => 'update_role',
        'delete_role' => 'delete_role',
        'delete_any_role' => 'delete_any_role',
    ],
    'roles' => [
        'admin' => ['backend_access'],
        'super_admin' => [
            'backend_access',
            'view_role',
            'view_any_role',
            'create_role',
            'update_role',
            'delete_role',
            'delete_any_role'
        ],
    ]
];
