<?php
namespace Database\Seeders;

use App\Models\User;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('users');

        if( env('FAKE_SEEDER_IS_ENABLED', false))
        {
            User::factory(15)->create();
        }
    }
}
