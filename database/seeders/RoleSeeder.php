<?php
namespace Database\Seeders;

use App\Traits\UtilPermissionRoleSeeder;
use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    use UtilSeeder;
    use UtilPermissionRoleSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('roles');

        if( class_exists('Spatie\Permission\Models\Role') )
        {
            $app_identity = env('APP_IDENTITY');

            // clean
            Artisan::call('cache:clear');

            // get all config files
            $files = [];
            $this->_list_config_files('./config/' . $app_identity, $files);


            foreach( $files as $file)
            {
                // get config key
                $config_key = $this->_config_file_to_config_key($app_identity, $file);

                // get values
                $refs = config($config_key . '.roles') ?? [];

                // iterate && create
                foreach ($refs as $role_name => $permissions)
                {
                    $role = Role::create(['name' => $role_name]);
                    $role->givePermissionTo($permissions);
                    $role->save();
                }
            }
        }
        else
        {
            \Log::error("[RoleSeeder:run] GPr01 No package spatie permission found. Install it!", []);
        }
    }
}

/*
fix : Spatie\Permission\Exceptions\RoleAlreadyExists
-> php artisan cache:clear
*/
