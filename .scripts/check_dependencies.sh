#!/bin/bash

PHP_VERSION="php8.2";

mods=(pdo_mysql mbstring tokenizer xml ctype json bcmath xdebug)

for mod in "${mods[@]}"
do
if php -m  | grep $mod > /dev/null
   then
       echo "V $mod already installed"

    else
       echo "! $mod will be installed"
       sudo aptitude install $PHP_VERSION-$mod > /dev/null

       if [ $? ]
       then
           echo "V $mod just became an installed"
       else
           echo "X $mod can not be installed"
       fi
   fi
done
