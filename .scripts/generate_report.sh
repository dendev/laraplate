#!/bin/bash

ROOT_PATH='./'

${ROOT_PATH}.scripts/generate_doc.sh
${ROOT_PATH}.scripts/generate_coverage.sh
${ROOT_PATH}.scripts/generate_metrics.sh

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ${ROOT_PATH}docs/index.html;
fi;

exit $?;
