#!/bin/bash

ROOT_PATH='./'

php ${ROOT_PATH}doctum.phar update ${ROOT_PATH}doctum.php;

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ${ROOT_PATH}docs/build/index.html;
fi;

exit $?;
