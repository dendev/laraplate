#!/bin/bash

ROOT_PATH='./'

# install if not exist
if ! whereis phpmetrics
then
    composer global require 'phpmetrics/phpmetrics'
fi

# use it
phpmetrics --report-html=${ROOT_PATH}docs/metrics ${ROOT_PATH}app --junit="${ROOT_PATH}phpunit.xml"

# optionaliy open result
if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ${ROOT_PATH}docs/metrics/index.html;
fi;

exit $?;
