#!/bin/bash

ROOT_PATH='./'

phpunit --coverage-html ${ROOT_PATH}docs/coverage &> /dev/null

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ${ROOT_PATH}docs/coverage/index.html;
fi;

exit $?;
